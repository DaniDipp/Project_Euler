package at.danidipp.euler;

public class Problem2 {
    public static void execute(){
        int sum = 0;

        int prev1;
        int prev2=0;
        for(int i=1; i<4000000; i += prev1){
            prev1 = prev2;
            prev2 = i;
            sum += i%2==0 ? i : 0;
        }

        System.out.println("Problem 2: " + sum);
    }
}
