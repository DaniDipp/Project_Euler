package at.danidipp.euler;

public class Problem1 {
    public static void execute(){
        int sum = 0;

        for(int i = 0; i<1000; i+=3){
            if(i%5==0) continue;
            sum += i;
        }
        for(int i = 0; i<1000; i+=5) sum += i;

        System.out.println("Problem 1: " + sum);
    }
}
