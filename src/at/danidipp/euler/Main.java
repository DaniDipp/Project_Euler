package at.danidipp.euler;

import java.util.Stack;

public class Main {

    public static void main(String[] args) {
        runAllProblems();
    }

    private static void runAllProblems() {
        final int PROBLEMS = 609; //06. Feb. 2018
        final int[] SKIP = {5, -1}; //Problems to skip in ascending order, with -1 at the bottom

        Stack<Integer> stack = new Stack();
        for (int i = SKIP.length-1; i>=0; i--) stack.push(SKIP[i]);

        int nextSkip = stack.pop();
        for (int i=1;i<=PROBLEMS;i++) {
            if(nextSkip == i){
                nextSkip = stack.pop();
                continue;
            }
            if(!runProblem(i)){
                System.err.println("Problem " + i + " not found; aborting.");
                break;
            }
        }
    }

    private static boolean runProblem(int i) {
        try {
            Class c = Class.forName("at.danidipp.euler.Problem"+i);
            c.getMethod("execute").invoke(Problem1.class);
            return true;
        } catch (Exception e) {
            //e.printStackTrace();
            return false;
        }
    }
}
