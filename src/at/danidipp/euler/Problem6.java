package at.danidipp.euler;

public class Problem6 {
    public static void execute() {
        int squareOfSum = 0;
        int sumOfSquares = 0;

        for(int i=1; i<=100; i++){
            squareOfSum += i;
            sumOfSquares += i*i;
        }
        squareOfSum *= squareOfSum;

        System.out.println("Problem 6: " + (squareOfSum - sumOfSquares));
    }
}