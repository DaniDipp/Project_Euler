package at.danidipp.euler;

import java.util.ArrayList;
import java.util.List;

public class Problem3 {
    public static void execute() {
        long n = 600851475143L;

        //Factorization by Lars Vogel: http://www.vogella.com/tutorials/JavaAlgorithmsPrimeFactorization/article.html
        List<Integer> factors = new ArrayList<>();
        for (int i = 2; i <= n; i++) {
            while (n % i == 0) {
                factors.add(i);
                n /= i;
            }
        }

        int max = 0;
        for(int i : factors){
            if(i>max) max=i;
        }

        System.out.println("Problem 3: " + max);
    }
}