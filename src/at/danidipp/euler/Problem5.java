package at.danidipp.euler;

public class Problem5 {
    public static void execute() {
        int num = 0;
        search: {
            for(int i=20; i<2432902008176640000L; i++) { // 20! = 2432902008176640000
                int a = 0;
                for(int j=20; j>0; j--){
                    if(i%j!=0)continue;
                    else{
                        a++;
                    }
                }
                if(a==20){
                    num = i;
                    break search;
                }
            }
        }
        System.out.println("Problem 5: " + num);
    }
}