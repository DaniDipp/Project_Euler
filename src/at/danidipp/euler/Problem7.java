package at.danidipp.euler;

public class Problem7 {
    public static void execute() {
        int lastchecked = 1;
        for(int i=1; i < 10001; i++){
            do lastchecked++; while(!isPrime(lastchecked));
        }
        System.out.println("Problem 7: " + lastchecked);
    }

    public static boolean isPrime(int num){
        if(num % 2 == 0) return false; //Skip even numbers
        int top = (int)Math.sqrt(num)+1; //Check up to the square of the number
        for(int i=3; i<top; i += 2){
            if(num % i == 0)return false;
        }
        return true;
    }
}