package at.danidipp.euler;

public class Problem4 {
    public static void execute() {
        int max = 0;
        for(int i=100; i<=999; i++){
            for(int j=100; j<=999; j++){
                int k = i * j;
                if(max < k && isPalindrome(k)) max = k;
            }
        }
        System.out.println("Problem 4: " + max);
    }

    private static boolean isPalindrome(int k) {
        char[] c = Integer.toString(k).toCharArray();
        int i1 = 0;
        int i2 = c.length - 1;
        while (i2 > i1) {
            if (c[i1] != c[i2]) {
                return false;
            }
            ++i1;
            --i2;
        }
        return true;
    }
}